package ru.volnenko.se.command.data;

import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Domain;

@Component
public class SaveJsonCommand extends AbstractCommand {

    @Override
    public String command() {
        return "data-save-json";
    }

    @Override
    public String description() {
        return "Save all data to json";
    }

    @Override
    public void execute() throws Exception {
        Domain domain = new Domain();
        domain.setProjects(projectService.getListProject());
        domain.setTasks(taskService.getListTask());
        dataService.saveJson(domain);
        System.out.println("[SAVED TO JSON]");
    }
}
