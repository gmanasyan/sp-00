package ru.volnenko.se.command.data;

import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Domain;

@Component
public class SaveXmlCommand extends AbstractCommand {

    @Override
    public String command() {
        return "data-save-xml";
    }

    @Override
    public String description() {
        return "Save all data to xml";
    }

    @Override
    public void execute() throws Exception {
        Domain domain = new Domain();
        domain.setProjects(projectService.getListProject());
        domain.setTasks(taskService.getListTask());
        dataService.saveXml(domain);
        System.out.println("[SAVED TO XML]");

    }
}
